package lesson3_homework.ab.com.lesson3_homework;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class AuthActivity extends AppCompatActivity {

    // static methods

    private static final String AUTH_KEY = "lesson3_homework.ab.com.lesson3_homework.AUTH_KEY";
    public static Intent getIntent(Context context, boolean hasSavedSettings) {
        Intent intent = new Intent(context, AuthActivity.class);
        intent.putExtra(AUTH_KEY, hasSavedSettings);
        return intent;
    }

    //

    private IPreferences mPreferences;
    private boolean hasSavedSettings;

    private TextView mTextEmail;
    private EditText mEditEmail;
    private EditText mEditPassword;
    private Button mButtonAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);
        init();
        setUp();
    }

    private void init() {
        mPreferences = MySharedPreferences.getInstance(this);
        mTextEmail = findViewById(R.id.text_email);
        mEditEmail = findViewById(R.id.edit_email);
        mEditPassword = findViewById(R.id.edit_password);
        mButtonAuth = findViewById(R.id.button_auth);
    }

    private void setUp() {
        Intent intent = getIntent();
        if (intent == null) {
            throw new IllegalArgumentException("AuthActivity should have been created using getIntent static method");
        }

        hasSavedSettings = intent.getBooleanExtra(AUTH_KEY, false);

        mButtonAuth.setText(hasSavedSettings ? R.string.SignIn : R.string.SignUp);
        mButtonAuth.setOnClickListener(this::onAuthClicked);

        mTextEmail.setText(hasSavedSettings ? mPreferences.getEmail() : "");
        mTextEmail.setVisibility(hasSavedSettings ? View.VISIBLE : View.GONE);

        mEditEmail.setVisibility(hasSavedSettings ? View.GONE : View.VISIBLE);

        if (hasSavedSettings) {
            mEditPassword.requestFocus();
        }
    }

    private void onAuthClicked(View view) {
        if (hasSavedSettings) {
            // check password and continue
            login();
        } else {
            // validate input data and register user
            registerUser();
        }
    }

    private void login() {
        final String enteredPassword = mEditPassword.getText().toString();
        final String storedPassword = mPreferences.getPassword();

        if (storedPassword.equals(enteredPassword)) {
            startActivity(MainActivity.getIntent(this));
            finish();
        } else {
            mEditPassword.setError("Invalid password!");
        }
    }

    private void registerUser() {

        String email = mEditEmail.getText().toString();
        String password = mEditPassword.getText().toString();

        if (isValidForRegistration(email, password)) {
            mPreferences.saveSettings(email, password);
            startActivity(MainActivity.getIntent(this));
            finish();
        }
    }

    private boolean isValidForRegistration(String email, String password) {
        return isValidEmail(email) && isValidPassword(password);
    }

    private boolean isValidEmail(String email) {
        if (email.isEmpty()) {
            mEditEmail.setError("Provide email address");
            return false;
        }

        if (!email.contains("@")) {
            mEditEmail.setError("Invalid email address provided");
            return false;
        }

        return true;
    }

    private boolean isValidPassword(String password) {
        if (password.isEmpty()) {
            mEditPassword.setError("Provide password");
            return false;
        }

        return true;
    }

}
