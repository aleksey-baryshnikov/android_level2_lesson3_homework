package lesson3_homework.ab.com.lesson3_homework;

import android.content.Context;
import android.content.SharedPreferences;

public class MySharedPreferences implements IPreferences {

    private static MySharedPreferences mInstance;

    private final String PREFIX = "lesson3_homework.ab.com.lesson3_homework.";
    private final String STORAGE_TITLE = PREFIX + "STORAGE_TITLE";
    private final String EMAIL_KEY = PREFIX + "EMAIL_KEY";
    private final String PASSWORD_KEY = PREFIX + "PASSWORD_KEY";

    private SharedPreferences mPreferences;

    private MySharedPreferences(Context context) {
        mPreferences = context.getSharedPreferences(STORAGE_TITLE, Context.MODE_PRIVATE);
    }

    public static IPreferences getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new MySharedPreferences(context);
        }

        return mInstance;
    }

    @Override
    public boolean hasSavedSettings() {
        return mPreferences.contains(EMAIL_KEY);
    }

    @Override
    public String getEmail() {
        return getStringValue(EMAIL_KEY);
    }

    @Override
    public String getPassword() {
        return getStringValue(PASSWORD_KEY);
    }

    @Override
    public void saveSettings(String email, String password) {
        execEditor(editor -> {
            editor.putString(EMAIL_KEY, email);
            editor.putString(PASSWORD_KEY, password);
        });
    }

    @Override
    public void clear() {
        execEditor(editor -> editor.clear());
    }

    private String getStringValue(String key) {
        return mPreferences.getString(key, "");
    }

    private void execEditor(PreferencesOperation call) {
        SharedPreferences.Editor editor = mPreferences.edit();
        call.operation(editor);
        editor.apply();
    }

    interface PreferencesOperation {
        void operation(SharedPreferences.Editor editor);
    }
}
