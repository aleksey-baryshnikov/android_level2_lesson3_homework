package lesson3_homework.ab.com.lesson3_homework;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    public static Intent getIntent(Context context) {
        return new Intent(context, MainActivity.class);
    }

    //

    private IPreferences mPreferences;
    private TextView mTextEmail;
    private Button mButtonSignOut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();
        setUp();
    }

    private void init() {
        mPreferences = MySharedPreferences.getInstance(this);
        mTextEmail = findViewById(R.id.text_email);
        mButtonSignOut = findViewById(R.id.button_sign_out);
    }

    private void setUp() {
        mTextEmail.setText(mPreferences.getEmail());
        mButtonSignOut.setOnClickListener(this::signOut);
    }

    private void signOut(View view) {
        mPreferences.clear();
        startActivity(AuthActivity.getIntent(this, false));
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_activity_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.itemProfile) {
            startActivity(ProfileActivity.getIntent(this));
            // no finish(); here as we want to be able to navigate back
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
