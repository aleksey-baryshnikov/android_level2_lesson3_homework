package lesson3_homework.ab.com.lesson3_homework;

public interface IPreferences {

    boolean hasSavedSettings();
    String getEmail();
    String getPassword();
    void saveSettings(String email, String password);
    void clear();

}
