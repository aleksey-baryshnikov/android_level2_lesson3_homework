package lesson3_homework.ab.com.lesson3_homework;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class ProfileActivity extends AppCompatActivity {

    public static Intent getIntent(Context context) {
        return new Intent(context, ProfileActivity.class);
    }

    //

    private IPreferences mPreferences;
    private TextView mTextEmail;
    private TextView mTextPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        init();
        setUp();
    }

    private void init() {
        mPreferences = MySharedPreferences.getInstance(this);
        mTextEmail = findViewById(R.id.text_email);
        mTextPassword = findViewById(R.id.text_password);
    }

    private void setUp() {
        mTextEmail.setText(mPreferences.getEmail());
        mTextPassword.setText(mPreferences.getPassword());
    }
}
